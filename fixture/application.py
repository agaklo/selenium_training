from selenium.webdriver.chrome.webdriver import WebDriver
from fixture.session import SessionHelper
from fixture.menu import MenuHelper
from fixture.countries import CountriesHelper
from fixture.products import ProductsHelper
from fixture.links import LinksHelper
from fixture.logs import LogsHelper


class Application:
    def __init__(self):
        self.wd = WebDriver()#(capabilities={"marionette": False})
#        self.wd.implicitly_wait(5)
        self.session = SessionHelper(self)
        self.menu = MenuHelper(self)
        self.countries = CountriesHelper(self)
        self.products = ProductsHelper(self)
        self.links = LinksHelper(self)
        self.logs = LogsHelper(self)

    def is_valid(self):
        try:
            self.wd.current_url
            return True
        except:
            return False

    def destroy(self):
        self.wd.quit()