import time


class CountriesHelper:

    def __init__(self, app):
        self.app = app


    def check_sorting_countries(self):
        wd = self.app.wd
        wd.find_element_by_xpath("//li[@id='app-']//span[.='Countries']").click()
        rows = wd.find_elements_by_class_name("row")
        country = []
        for row in rows:
            country.append(row.find_element_by_css_selector("td a").get_attribute("innerText"))
        if sorted(country) == country:
            print("list sorted")
        else:
            print("list is not sorted")



    def check_zones(self):
        wd = self.app.wd
        wd.find_element_by_xpath("//span[contains(text(),'Countries')]").click()
        rows = len(wd.find_elements_by_css_selector(".row"))
        for index in range(rows):
            if (wd.find_elements_by_css_selector("td:nth-child(6)")[index].get_attribute("textContent")) > str("0"):
                wd.find_elements_by_css_selector("[title *= Edit]")[index].click()
                time.sleep(3)
                table = wd.find_elements_by_xpath("//td/input[contains(@name,'][name]')]")
                list = []
                for i in table:
                    list.append(i.get_attribute("defaultValue"))
                if sorted(list) == list:
                    print("list sorted")
                else:
                    print("list is not sorted")
                time.sleep(3)
                wd.find_element_by_xpath("//span/button[contains(@name,'cancel')]").click()


    def check_geo_zones(self):
        wd = self.app.wd
        wd.find_element_by_xpath("//span[contains(text(),'Geo Zones')]").click()
        rows = len(wd.find_elements_by_css_selector(".row"))
        for index in range(rows):
            wd.find_elements_by_css_selector("[title *= Edit]")[index].click()
            time.sleep(1)
            selected = wd.find_elements_by_xpath("//select[contains(@name,'zone_code')]/option[contains(@selected, 'selected')]")
            list2 = []
            for j in selected:
                list2.append(j.get_attribute("textContent"))
            if sorted(list2) == list2:
                print("list sorted")
            else:
                print("list is not sorted")
            time.sleep(1)
            wd.find_element_by_xpath("//span/button[contains(@name,'cancel')]").click()


