import time

class MenuHelper:

    def __init__(self, app):
        self.app = app

    def click_menu_items(self):
        wd = self.app.wd
        menu = wd.find_elements_by_xpath("//li[@id='app-']")
        next = len(menu) + 1
        for i in range(1, next):
            wd.find_element_by_css_selector("li#app-:nth-child(%d)" % i).click()
            wd.find_element_by_tag_name('h1')
            items = wd.find_elements_by_xpath("//li[contains(@id,'doc-')]")
            next_item = len(items) + 1
            for mini_item in range(1, next_item):
                wd.find_element_by_css_selector(".docs :nth-child(%d)" % mini_item).click()
                wd.find_element_by_tag_name('h1')
                i += 1


