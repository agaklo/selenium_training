from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


class LinksHelper:

    def __init__(self, app):
        self.app = app


    def open_links(self):
        wd = self.app.wd
        wd.find_element_by_xpath("//li[@id='app-']//span[.='Countries']").click()
        wd.find_element_by_css_selector("[title *= Edit]").click()
        links = wd.find_elements_by_xpath("//i[@class='fa fa-external-link']")
        for i in links:
            window_before = wd.window_handles[0]
            wait = WebDriverWait(wd, 10)
            i.click()
            wait.until(EC.visibility_of(i))
            window_after = wd.window_handles[1]
            wd.switch_to_window(window_after)
            wd.close()
            wd.switch_to_window(window_before)
            time.sleep(1)