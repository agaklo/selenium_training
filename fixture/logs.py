
class LogsHelper:

    def __init__(self, app):
        self.app = app


    def checking_logs(self):
        wd = self.app.wd
        wd.find_element_by_xpath("//li[@id='app-']//span[.='Catalog']").click()
        wd.find_element_by_xpath("//a[@href='http://localhost/litecart/admin/?app=catalog&doc=catalog&category_id=1']").click()
        products = len(wd.find_elements_by_xpath("//a[contains(@href,'doc=edit_product&category_id=1&product_id')][@title = 'Edit']"))
        for index in range(products):
            wd.find_elements_by_xpath("//a[contains(@href,'doc=edit_product&category_id=1&product_id')][@title = 'Edit']")[index].click()
 #           for l in wd.get_log("browser"):
 #               print(l)
            logs = wd.get_log("browser")
            messages = map(lambda l: l["message"], logs)
            has_console_logs = any(map(lambda m: m.find("console log") >= 0, messages))
            print("Success" if has_console_logs else "Failure")
            wd.find_element_by_xpath("//button[@name='cancel']").click()
