import time

class SessionHelper:

    def __init__(self, app):
        self.app = app

    def login(self, username, password):
        wd = self.app.wd
        wd.get("http://localhost/litecart/admin/")
        wd.find_element_by_name("username").send_keys(username)
        wd.find_element_by_name("password").send_keys(password)
        wd.find_element_by_name("login").click()
        time.sleep(1)

    def logout(self):
        wd = self.app.wd
        wd.find_element_by_css_selector('[title=Logout]').click()

    def ensure_logout(self):
        wd = self.app.wd
        if len(wd.find_elements_by_css_selector('[title=Logout]')) > 0:
            self.logout()