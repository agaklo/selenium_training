from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def test_example(app, wd):
    wd.implicitly_wait(10)
    wd.get("http://www.google.pl/")
    wd.find_element_by_name("q").send_keys("webdriver")
    wd.find_element_by_name("q").send_keys(Keys.ESCAPE)
    wd.find_element_by_name("btnK").click()
    WebDriverWait(wd, 10).until(EC.title_is("webdriver - Szukaj w Google"))
